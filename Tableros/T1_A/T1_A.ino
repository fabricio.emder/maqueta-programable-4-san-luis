/*
   Tablero 1, placa A.
   ver: 1.1.0
   //tren
   Acciones:
   101 ,102 Luminarias autopista, out 1 y 2.

   50 Cargar/descargar el tren A, lleva tren a posicion de brazo A y lo activa.
   51 Cargar/descargar el tren B, lleva tren a posicion de brazo B y lo activa.
   53 Prender Tren de estacion productividad. 0,1;
   53 Params:
   21, 22: Manual ir, 23,24 Manual Volver, 20 Manual STOP;

   Ir a estacion:
   param:
   30 estacion 0, productividad
   31 estacion 1, beazo 1
   32 estacion 2, brazo 2
   33 estacion 3, v mcds

   sensores en vmcsd
   parms: 10 y 11
*/
#define ETHERCON
#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>
#include <avr/wdt.h>


#define TREN_SLOW 135
#define TREN_FAST 255
#define TREN_STOP 0
#define SENS_UMBRAL 250
#define SENS_HIST 100 // activa en umbral deactiva en humbral - hist.

#define FRENAR 1
#define IR 2
#define VOLVER 3
#define APAGADO 0

TCPcomm TCP;
NANO_EXPANDER Brd;

uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x01};

#define ACTION_COUNT 8
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {101, 102, 50, 51, 53, 86, 120, 121};

boolean sensorSet[6] = {false, false, false, false, false, false};
byte sensorPins[6] = {A0, A1, A2, A3, A6, A7};
boolean trenDir = false;
int trenVel = 0;
byte estacionAct = 3;
byte estacionDest = 3;
byte estadoTren = APAGADO;
byte llegoDest = false;
unsigned long trenTimer = 0;
boolean trenTimerOn = false;
#define VMCDS_TIME 5000

bool armsReady = true;

void setup() {
  wdt_disable();

  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
#endif
  /// mac, Arduino Ip, Server Ip, puerto
  TCP.init(mac, IPAddress(10, 75, 33, 111), IPAddress(10, 75, 33, 101), 9001);
  Brd.init();

  tren();
  irEstacion(0);
  wdt_enable(WDTO_2S);
}

void loop() {
  wdt_reset();

  //Recibir mensajes del server.
  //esta funcion actualiza el valor de actionState[x] con el parametro recibido segun el correspondiente numero de actionNumber[x].
  TCP.parse(actionState, actionNumber, ACTION_COUNT);
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);

  tren();

  //  Acciones :
  //Luces de autopista
  Brd.setOut(1, actionState[0] * 255);
  Brd.setOut(2, actionState[1] * 255);

  //las acciones 6 y 7 (120 y 121) reflejan el estado de ejecucion de la rutina de brazos. 1 en proceso 0 terminada.
  armsReady = actionState[6] + actionState[7] == 0;

  //tren BRAZOS! las acciones de brazos tren llegan a tren.
  switch (actionState[2]) {
    case 0:
      //IDLE
      break;
    case 1:
      actionState[3] = 0; //anular llamado del otro brazo
      //enviar tren a estacion brazo 1, productividad.
      if (armsReady) {
        irEstacion(1);
        actionState[2] = 2;
      }
      break;
    case 2:
      if (llegoDest) {
        actionState[2] = 3;
        actionState[6] = 1;
        Brd.sendAction(120, 1);
      }
      break;
    case 3:
      //esperar brazo.
      break;
  }

  switch (actionState[3]) {
    case 0:
      //IDLE
      break;
    case 1:
      actionState[2] = 0; //anular llamado del otro brazo
      //enviar tren a estacion brazo 2, productividad.
      if (armsReady) {
        irEstacion(2);
        actionState[3] = 2;
      }
      break;
    case 2:
      if (llegoDest) {
        actionState[3] = 3;
        actionState[7] = 1;
        Brd.sendAction(121, 1);
      }
      break;
    case 3:
      //esperar brazo.
      if (actionState[7] == 0) {
        actionState[3] = 0;
        irEstacion(3);
      }
      break;
  }

  //Tren
  switch (actionState[4]) {
    case 0:
      //IDLE
      break;

    //sensores en Vmcd,
    case 10:
      //snens slow mercedes
      Serial.println("sens 6");
      if (trenDir) {
        velocidad(TREN_SLOW);
      }
      break;

    case 11:
      //snens fin mercedes
      Serial.println("sens 7");
      estacionAct = 3;
      trenTimer = millis();
      trenTimerOn = true;
      break;

    //manual
    case 20:
      controlTren(FRENAR);
      break;
    case 21:
      controlTren(IR);
      velocidad(TREN_SLOW);
      break;
    case 22:
      controlTren(IR);
      break;
    case 23:
      controlTren(VOLVER);
      velocidad(TREN_SLOW);
      break;
    case 24:
      controlTren(VOLVER);
      break;
    case 30:
      irEstacion(0);
      break;
    case 31:
      irEstacion(1);
      break;
    case 32:
      irEstacion(2);
      break;
    case 33:
      irEstacion(3);
      break;
  }
  actionState[4] = 0;

  if (trenTimerOn) {
    if (millis() - trenTimer > VMCDS_TIME) {
      trenTimerOn = false;
      irEstacion(0);
    }
  }
}



void irEstacion(byte dest) {
  Serial.print("ir a: ");
  Serial.println(dest);

  if (dest != estacionDest ) {
    estacionDest = dest;
    llegoDest = false;
    velocidad(TREN_FAST);
  }
  Serial.print("ACT: ");
  Serial.print(estacionAct);
  Serial.print(" | DEST: ");
  Serial.print(estacionDest);

  if ((estacionAct - estacionDest) > 0) {
    controlTren(IR);
  } else if ((estacionAct - estacionDest) < 0) {
    controlTren(VOLVER);
  }
}
void tren() {
  if(!llegoDest) sensoresTren();

  if (estacionDest == estacionAct && !llegoDest ) {
    llegoDest = true;
    controlTren(FRENAR);
    controlTren(APAGADO);
  }
}

void sensoresTren() {
  for (byte i = 0; i < 6; i++) {
    int sensVal = analogRead(sensorPins[i]);
    //        Serial.print(i);
    //        Serial.print(" = ");
    //        Serial.print(sensVal);
    //        Serial.print(" | ");
    if (sensVal < SENS_UMBRAL - SENS_HIST) sensorSet[i] = false;
    //    Serial.print(i);
    //    Serial.print(" = ");
    //    Serial.print(sensorSet[i]);
    //    Serial.print(" | ");
  }
  //Serial.println();

  //Sens 0 final productividad
  if ((analogRead(A0) > SENS_UMBRAL) && !sensorSet[0]) {
    sensorSet[0] = true;
    Serial.println("sens 0");
    if (!trenDir) velocidad(TREN_STOP);
    estacionAct = 0;
  }

  //Sens 1 salida productividad / stop al volver brazo 1
  if ((analogRead(A1) > SENS_UMBRAL) && !sensorSet[1]) {
    sensorSet[1] = true;
    Serial.println("sens 1");
    //siempre que entra a 0 es en slow
    if (estacionDest == 0 && !trenDir) velocidad(TREN_SLOW);
    // si esta volviendo y el destino es brazo uno, llego.
    if (!trenDir) estacionAct = 1;
  }

  //Sens 2 parada vuelta brazo 2 / slow parada ida brz 1.
  if ((analogRead(A2) > SENS_UMBRAL) && !sensorSet[2]) {
    sensorSet[2] = true;
    Serial.println("sens 2");
    // si esta volviendo a 2 hace stop
    if (!trenDir) estacionAct = 2;
    // si esta yendo o volviendo a 1 hace slow
    if (estacionDest == 1) velocidad(TREN_SLOW);
  }

  //Sens 3 parada vuelta brazo 2 / slow parada ida brz 1.
  if ((analogRead(A3) > SENS_UMBRAL) && !sensorSet[3]) {
    sensorSet[3] = true;
    Serial.println("sens 3");
    // si esta yendo o volviendo a 2 hace slow.
    if (estacionDest == 2) velocidad(TREN_SLOW);
    // si esta yendo a 1 hace stop
    if (trenDir) estacionAct = 1;
  }

  //Sens 4 parada ida brazo 2 / slow curva
  if ((analogRead(A6) > SENS_UMBRAL) && !sensorSet[4]) {
    sensorSet[4] = true;
    Serial.println("sens 4");
    // si esta yendo a 2 hace stop.
    if (trenDir) estacionAct = 2;
    // si esta yendo a 3 hace slow por la curva.
    if (estacionDest == 3) velocidad(TREN_SLOW);
  }

  //Sens 5 parada ida salida entrada brazos
  if ((analogRead(A7) > SENS_UMBRAL) && !sensorSet[5]) {
    sensorSet[5] = true;
    Serial.println("sens 5");
    // si esta entrando a brazos hace slow por la curva
    if (estacionDest < 3) velocidad(TREN_SLOW);
    // si esta yendo a 3 hace slow por la curva.
    if (estacionDest == 3) velocidad(TREN_FAST);
  }
}

void velocidad(byte vel) {
  trenVel = vel;
  controlTren(estadoTren);
}

void controlTren(byte state) {
  Serial.print(" | VEL: ");
  Serial.println(trenVel);
  Serial.print(" | Estado: ");
  Serial.println(estadoTren);

  estadoTren = state;
  switch (state) {
    case 0:
      //nada
      break;
    case 1:
      //frenar
      Brd.setOut(4, 0);
      Brd.setOut(5, 0);
      break;
    case 2:
      //volver
      trenDir = false;
      Brd.setOut(4, 0);
      Brd.setOut(5, trenVel);
      break;
    case 3:
      //ir
      trenDir = true;
      Brd.setOut(4, trenVel);
      Brd.setOut(5, 0);
      break;
  }
}
