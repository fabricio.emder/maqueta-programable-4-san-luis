/*
   Tablero 3, placa A.
   ver: 1.0.0
   Acciones:
   -- GENERAL
   2 Autos autopista
   105 y 106 luces autopista
    --casa futuro
  73  Encendido de panel solar o4

*/

#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>
#include <avr/wdt.h>


TCPcomm TCP;
NANO_EXPANDER Brd;

#define ACTION_COUNT 5
uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x03};
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {105, 106, 2, 73, 200};


//int autoSpeed = 50;
//int sensorPinA = A0;
//int sensorValueA = 0;

unsigned long timerPanel;
#define VEL_PANEL 100
#define T_PANEL 1500

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
#endif
  //pinMode(4, OUTPUT);
  //digitalWrite(4, HIGH);
  /// mac, Arduino Ip, Server Ip
  TCP.init(mac, IPAddress(10, 75, 33, 113), IPAddress(10, 75, 33, 101), 9001);
  Brd.init();
  timerPanel = millis();
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  TCP.parse(actionState, actionNumber, ACTION_COUNT);

  //luces autopista
  Brd.setOut(1, actionState[0] * 255);
  Brd.setOut(2, actionState[1] * 255);
  //autos autopista
  Brd.setOut(3, actionState[2] * 255);
  //paneles
  //paneles actionState[3]
  //s 0 = mover cerrar y cont
  //s1 mover abrir y cont
  //s2 esprar y parar
  //s3 idle
  if (actionState[3] > 0 && actionState[3] < 100) {
    timerPanel = millis();
    Brd.setOut(4, VEL_PANEL);
    actionState[3] = 100;
  } else if (actionState[3] == 100) {
    if (millis() - timerPanel > T_PANEL) {
      Brd.setOut(4, 0);
      actionState[3] = 0;
    }
  } else {
    Brd.setOut(4, 0);
  }
}
