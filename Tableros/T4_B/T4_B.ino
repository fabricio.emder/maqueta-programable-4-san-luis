/*
 * DIQUE APAGADO EN NEGRO, CAMBIAR A ROJO
 * 
  // Tablero 4 palaca B
  ver: 1.0.0
   Acciones:
   --Merlo
   43 alumbrado publico
   44 alumbrado casas
   45 pantalla poemas
   48 globo
   49 auto garage

    --Hidrica
   66 guarderia (i2c 0)
   68 dique

  //
*/
#include <Gallardcore_NanoExpander.h>
#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>


NANO_EXPANDER Brd;
//Desplazamiento(uint8_t homeSensorPin, uint8_t endSensorPin, uint8_t motorPinA, uint8_t motorPinB, uint8_t speed){

Desplazamiento garage(A1, A0, OUT3, OUT4 , 255);

#define ACTION_COUNT 7
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {49, 48, 43, 44, 45, 66, 68};

unsigned long timerGlobo = millis();


#define PXLED_PIN 2 //px Out 2
#define LED_COUNT 4

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800); //fuente

Adafruit_PWMServoDriver Srv = Adafruit_PWMServoDriver();

#define SRV_CONT_MAX 200
#define SRV_CONT_MIN 500
#define SRV_CONT_MID 350
#define T_SRV_CONT 900
unsigned long timerSrvCont;

bool yaEnCero;

void setup() {
  wdt_disable();

#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif
  Brd.init();

  // servo
  Srv.begin();
  Srv.setPWMFreq(60);
  timerSrvCont = millis();
  Srv.setPWM(0, 0, 350);

  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP


  if (analogRead(A2) > 200) {
    yaEnCero = true;
  } else {
    yaEnCero = false;
  }
  wdt_enable(WDTO_2S);

}

void loop() {
  wdt_reset();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  garage.tick();
  //poema
  if (actionState[4] > 0) {
    digitalWrite(13, LOW);
  } else {
    digitalWrite(13, HIGH);

  }
  //garage
  if (actionState[0] > 0) {
    actionState[0] = 0;
    garage.go();
  }

  //globo
  switch (actionState[1]) {
    case 0:
      if (!yaEnCero) {
        if (analogRead(A2) > 200) {
          Brd.setOut(5, 0);
          Brd.setOut(6, 0);
          yaEnCero = true;
        } else {
          Brd.setOut(5, 255);
          Brd.setOut(6, 0);
        }
      }
      break;
    case 1:
      timerGlobo = millis();
      actionState[1] = 2;
      yaEnCero = false;
      break;
    case 2:
      if (millis() - timerGlobo > 30000 || millis() - timerGlobo < 0) {
        actionState[1] = 0;
      }
      Brd.setOut(5, 0);
      Brd.setOut(6, 255);    
      break;
    
  }

  //luces cabañas merlo
  Brd.setOut(1, actionState[3] * 255);

  //luces alumbrado publico merlo
  Brd.setOut(2, actionState[2] * 255);

  //Luces dique
  if (actionState[6] > 0) {
    for (byte i = 0; i < 4; i++) {
      px1.setPixelColor(i, px1.Color(0,   255,   255));
    }
  } else {
    for (byte i = 0; i < 4; i++) {
      px1.setPixelColor(i, px1.Color(0,   0,   0));
    }
  }

  px1.show();

  //Guarderia bote
  //actionState[5]
  //s 0 = mover cerrar y cont
  //s1 mover abrir y cont
  //s2 esprar y parar
  //s3 idle
  switch (actionState[5]) {
    case 0:
      timerSrvCont = millis();
      Srv.setPWM(0, 0, SRV_CONT_MIN);
      actionState[5] = 2;
      break;
    case 1:
      timerSrvCont = millis();
      Srv.setPWM(0, 0, SRV_CONT_MAX);
      actionState[5] = 2;
      break;
    case 2:
      if (millis() - timerSrvCont > T_SRV_CONT) {
        Srv.setPWM(0, 0, SRV_CONT_MID);
        actionState[5] = 3;
      }
      break;
    case 3:
      //HOLA QUE TAL, ESTOY ACA HACIENDO NADA
      break;
  }


}
