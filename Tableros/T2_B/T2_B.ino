/*
   Tablero 2, placa B.
   ver: 1.0.0
   Acciones:
   -- aeropuerto
   34 avion o4 o5 a1 a0
   35 pista px1
   36 luz aeropuerto o6
   37 radar o3
   //Luces de autopista
   103 y 104 o1 y o2

*/

#include <Gallardcore_NanoExpander.h>
#include <Adafruit_NeoPixel_NOT.h>
#include <avr/wdt.h>

NANO_EXPANDER Brd;

#define PXLED_PIN 7 //px Out 1
#define LED_COUNT 18

Adafruit_NeoPixel px1(LED_COUNT, PXLED_PIN, NEO_BRG + NEO_KHZ800); //fuente

Desplazamiento avion(A1, A0, OUT5, OUT4, 255);

#define ACTION_COUNT 6
uint8_t actionState[ACTION_COUNT] = {0, 0, 0, 0, 0, 0};
static uint8_t actionNumber[ACTION_COUNT] = {103, 104, 36, 37, 35, 34};

unsigned long timer = 0;
const unsigned int frameTime = 70;
unsigned long frame = 0;

byte fxStep = 0;

void setup() {
  wdt_disable();
#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  Brd.init();

  //px out1
  px1.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  px1.setBrightness(255); //
  px1.show();            // Turn OFF all pixels ASAP
  wdt_enable(WDTO_2S);
}

void loop() {
  wdt_reset();
  frameCounter();
  Brd.parseSerial(actionState, actionNumber, ACTION_COUNT);
  avion.tick();
  //Luces de autopista
  Brd.setOut(1, actionState[0] * 255);
  Brd.setOut(2, actionState[1] * 255);

  //Radar
  Brd.setOut(3, actionState[3] * 50);

  //Luz aeropuerto
  Brd.setOut(6, actionState[2] * 255);

  //Luces pista
  lucesPista(actionState[4]);

  //avion
  if (actionState[5] > 0) {
    actionState[5] = 0;
    avion.go();
  }
  px1.show();
}

void lucesPista(byte state) {
  if (state > 0) {
    const byte animSteps = 9;
    // byte fxStep = frame % animSteps;
    if (frame > 4-state) {
      fxStep++;
      frame = 0;
    }
    if (fxStep > animSteps) fxStep = 0;

    for (int i = 0; i < animSteps; i++) {
      if (fxStep == i) {
        px1.setPixelColor(i, px1.Color(255,   255,   255));
        px1.setPixelColor(17 - i, px1.Color(255,   255,   255));
      } else {
        px1.setPixelColor(i, px1.Color(0,   50,   0));
        px1.setPixelColor(17 - i, px1.Color(0,   50,   0));
      }
    }
  } else {
    for (int i = 0; i < LED_COUNT; i++) {
      px1.setPixelColor(i, 0);
    }
  }
}

void frameCounter() {
  if (millis() - timer > frameTime) {
    timer = millis();
    frame++;
  }
}
