/*!
Protocolo de comunicacion por TCP para control de acciones
Mensaje tipo:

Inicio de mensaje (~# 2 bytes) Tipo de mensaje (1 byte) # Accion (1 byte) Parametros (n Bytes)

Tipo de mensaje:
0 = Mensaje de canal (mensajes entre dispositivos)
1 = Mensaje de accion (recibe numero de accion y parametros)
2 = Mensaje de estado (envia el estado de una accion)

 */

#include "Arduino.h"
#include <UIPEthernet.h>
#include "Gallardcore_TCP_Protocol.h"
#include "HardwareSerial.h"

 #include <UIPEthernet.h>
 #include "utility/logging.h"

 EthernetClient client;

TCPcomm::TCPcomm()
{
}

TCPcomm::init(uint8_t *mac,IPAddress myIp,IPAddress serverIp,uint16_t port){
    // iniciar placa de red
    _serverIp = serverIp;
    _port = port;
    _next = 0;

    Ethernet.init(8); //SS pin para NanoExpander
    Ethernet.begin(mac,myIp); //Configura mac and IP

    #if ACTLOGLEVEL>=LOG_INFO
    //log de conexion
      LogObject.uart_send_str(F("localIP: "));
      LogObject.println(Ethernet.localIP());

      LogObject.uart_send_str(F("subnetMask: "));
      LogObject.println(Ethernet.subnetMask());

      LogObject.uart_send_str(F("gatewayIP: "));

      LogObject.println(Ethernet.gatewayIP());

      LogObject.uart_send_str(F("dnsServerIP: "));

      LogObject.println(Ethernet.dnsServerIP());
    #endif

    connect(); //conectarse al server TCP
    return client.connected();
}

TCPcomm::connect(){
    // funcion de conexion con timer de 5 segundos
    if(millis()-_next > 5000){
      _next = millis();
      #if ACTLOGLEVEL>=LOG_INFO
      //log de conexion
      LogObject.uart_send_strln(F("Conectando.. "));

        LogObject.uart_send_str(F("serverIP: "));
        LogObject.println(_serverIp);

        LogObject.uart_send_str(F("port: "));
        LogObject.println(_port);


      #endif

      if (client.connect(_serverIp, _port))
      {
        #if ACTLOGLEVEL>=LOG_INFO
          LogObject.println(client.connected());
        #endif
        //client.println("HI");
      } else {
        #if ACTLOGLEVEL>=LOG_INFO
          LogObject.println(client.connected());
        #endif
      }
    }
}

TCPcomm::parse(uint8_t *stateBuf,uint8_t *actionNumbers,uint8_t actionCount)
{
  //interprete de mensajes
  if(client.connected() > 0){
    int size;
    while((size = client.available()) > 0)
            {
              uint8_t* msg = (uint8_t*)malloc(size+1);
              memset(msg, 0, size+1);
              size = client.read(msg,size);

              #if ACTLOGLEVEL>=LOG_INFO
              //    LogObject.write(msg,size);
              //    LogObject.println();
              #endif

              for(uint8_t i = 0; i < size;i++){
                Serial.write(msg[i]);
              }

              if(msg[0] == '~' && msg[1] == '#'){
                  if (msg[2] == 1){

                //  LogObject.println(sizeof(actionNumbers));

  //                for(uint8_t i = 0; i < (sizeof(actionNumbers) / sizeof(actionNumbers[0]));i++){
                    for(uint8_t i = 0; i < actionCount;i++){

                    if(actionNumbers[i] == msg[3]){
                      stateBuf[i] = (uint8_t) msg[4];

                      #if ACTLOGLEVEL>=LOG_INFO
                        LogObject.println();
                        LogObject.print(F("ACTION_MSJ: "));
                        LogObject.print(msg[3]);
                        LogObject.print(F(" PARAM: "));
                        LogObject.print(msg[4]);
                        LogObject.println();

                      #endif
                    }
                  }
                }
              }

              free(msg);

            }
  } else{
    connect();
  }
}

TCPcomm::sendAction(uint8_t actNum, uint8_t parm){
  client.print("~#");
  client.write(1);
  client.write(actNum);
  client.write(parm);
}
