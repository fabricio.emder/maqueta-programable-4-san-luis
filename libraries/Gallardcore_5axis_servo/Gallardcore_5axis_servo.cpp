
/*
5 axis servo control with easing

*/

#include "Gallardcore_5axis_servo.h"

Axis_Control::init(Adafruit_PWMServoDriver& servo,const uint8_t *rutine){
  for(uint8_t i = 0; i < 5 ;i++){
    uint8_t val = rutine[i];
    Serial.print(val);
    setAxis(i,val);
    _setPos[i]= val;
    _currentPos[i] = val;

  }
  Serial.println();
  _servo = servo;
  _servo.begin();
  _servo.setPWMFreq(60);  // Analog servos run at ~60 Hz updates
  goToPosition();
}

Axis_Control::setAxis(uint8_t axis,uint8_t pos){
  _setPos[axis] = pos;
}

Axis_Control::goToPosition(){
  _easingDone = false;
}

Axis_Control::rutineState(){
  return _rutineDone;
}

Axis_Control::updateAxis(){
  if (!_easingDone) easingPos();

  if(!_rutineDone && _easingDone){
    if (millis() - _rutineTimer > STEP_TIME || millis() - _rutineTimer < 0){
      _rutineTimer = millis();

      for(uint8_t i = 0; i < 5 ;i++){
        setAxis(i,_rutine[_rutineStep * 5 + i]);
        Serial.print(_rutine[_rutineStep * 5 + i]);
        Serial.print(" ");
      }
      Serial.println();
      goToPosition();

      _rutineStep ++;
      if(_rutineStep > _rutineStepCount - 1) _rutineDone = true;
    }
  }
}

Axis_Control::doRutine(const uint8_t *rutine, uint8_t stepCount){
  _rutineStep = 0;
  _rutineStepCount = stepCount;
  _rutine = rutine;
  _rutineDone = false;
}

Axis_Control::easingPos() {
  if(_easingCounter < COUNTER_MAX) {
    for (byte eje = 0; eje < 5; eje++) {
      // para cada eje
      byte pos = Easing::easeInOutCubic(_easingCounter, _currentPos[eje],  _setPos[eje] - _currentPos[eje] , COUNTER_MAX);
      setServo(eje, pos);
    }
    _easingCounter ++;
  } else {
    // Serial.println("easingDone");
    _easingCounter = 0;
    _easingDone = true;
    for (byte eje = 0; eje < 5; eje++) {
      // para cada eje
      _currentPos[eje] = _setPos[eje];
    }
  }
}

Axis_Control:: setServo(byte num, byte angle) {

  if (angle >= _minLimit[num] && angle <= _maxLimit[num]) {
    _servo.setPWM(_servoPins[num], 0, map(angle, 0, 180, 170, 559));
    delay(1);
  } else {
    //Serial.println("fuera de rango");
  }
}
