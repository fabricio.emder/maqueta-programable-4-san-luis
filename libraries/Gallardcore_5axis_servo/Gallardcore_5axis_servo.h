#ifndef Gallardcore_5axis_servo_h
#define Gallardcore_5axis_servo_h

#include "Easing.h"
#include "Adafruit_PWMServoDriver.h"


#define AX1 5
#define AX2 1
#define AX3 2
#define AX4 3
#define AX5 4

#define STEP_TIME 1500
#define COUNTER_MAX 250


class Axis_Control
{
public:
  init(Adafruit_PWMServoDriver& servo, const uint8_t *rutine);
  setAxis(uint8_t axis, uint8_t pos);
  goToPosition();
  doRutine(const uint8_t *rutine, uint8_t stepCount);
  updateAxis();
  rutineState();
private:
  easingPos();
  setServo(uint8_t num, uint8_t angle);

  uint8_t _servoPins[5] = {AX1,AX2,AX3,AX4,AX5};
  uint16_t _easingCounter = 0;
  bool _easingDone = true;
  uint8_t _minLimit[5] = {0,15,0,0,50};
  uint8_t _maxLimit[5] = {180,150,90,180,180};
  uint8_t _setPos[5]= {90, 120, 50, 90, 100};
  uint8_t _currentPos[5] = {90, 120, 50, 90, 100};
  Adafruit_PWMServoDriver _servo;
  uint8_t _rutineStep = 0;
  uint32_t _rutineTimer = 0;
  uint8_t* _rutine;
  bool _rutineDone = true;
  uint8_t _rutineStepCount = 0;

};

#endif
