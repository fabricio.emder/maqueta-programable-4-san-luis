/*   Brazorg
   Programa para control de los brazos roboticos en tiempo real por usb.
   Recibe mensaje: ~#Nxxxxx donde N es la letra en mayus de cada brazo (A,B,C) y
   x son 5 bytes que representan la posicion en grados de 0 a 180 de cada eje
   en el sig orden: base, hombro, codo, rot garra, garra
   Ver parte de serie y elegir a que brazo esta mandando.
   Vel de comunicacion 9600.
*/

#include "rutinas.h"
#include <Easing.h>

#include <Wire.h>
#include <Gallardcore_5axis_servo.h>
#include <Adafruit_PWMServoDriver.h>

Adafruit_PWMServoDriver servoA = Adafruit_PWMServoDriver(0x41);// 0x42 es el 1 (tren mas cercano a productividad)
Adafruit_PWMServoDriver servoB = Adafruit_PWMServoDriver(0x40);// 0x40 es el 2 (tren mas cercano a san luis)
Adafruit_PWMServoDriver servoC = Adafruit_PWMServoDriver(0x42);// 0x41 es el 3 (independiente)

Axis_Control arm_A;
Axis_Control arm_B;
Axis_Control arm_C;

boolean cargadoA = false;
boolean cargadoB = false;
boolean cargadoC = false;

void setup() {
  Serial.begin(9600);
  Serial.println("5 axis Serial test");

  //init: instancia del servo driver i2c, posicion de inicio.
  arm_A.init(servoA, &cargaBrazoA[0][0]);
  arm_B.init(servoB, &cargaBrazoB[0][0]);
  arm_C.init(servoC, &cargaBrazoC[0][0]);

}

void loop() {
  arm_A.updateAxis();
  arm_B.updateAxis();
  arm_C.updateAxis();

  //Descomentar para activar rutinas.

  //      if (arm_A.rutineState()) {
  //        if (!cargadoA) {
  //          cargadoA = true;
  //          arm_A.doRutine(&cargaBrazoA[0][0], 11);
  //        } else {
  //          cargadoA = false;
  //          arm_A.doRutine(&descargaBrazoA[0][0], 11);
  //        }
  //      }
  //
  //        if (arm_B.rutineState()) {
  //          if (!cargadoB) {
  //            cargadoB = true;
  //            arm_B.doRutine(&cargaBrazoB[0][0], 11);
  //          } else {
  //            cargadoB = false;
  //            arm_B.doRutine(&descargaBrazoB[0][0], 11);
  //          }
  //        }
  //
  //      if (arm_C.rutineState()) {
  //        if (!cargadoC) {
  //          cargadoC = true;
  //          arm_C.doRutine(&cargaBrazoC[0][0], 11);
  //        } else {
  //          cargadoC = false;
  //          arm_C.doRutine(&descargaBrazoC[0][0], 11);
  //        }
  //      }

  if (Serial.available() > 2) {
    if (Serial.read() == '~') {
      if (Serial.read() == '#') {
        byte armNumb = Serial.read();
        delay(10);
        for (byte i = 0; i < 5; i++) {
          //setServo(i, Serial.read());
          switch (armNumb) {
            case 'A':
              arm_A.setAxis(i, Serial.read());
              break;
            case 'B':
              arm_B.setAxis(i, Serial.read());
              break;
            case 'C':
              arm_C.setAxis(i, Serial.read());
              break;
          }
        }

        switch (armNumb) {
          case 'A':
            arm_A.goToPosition();
            break;
          case 'B':
            arm_B.goToPosition();
            break;
          case 'C':
            arm_C.goToPosition();
            break;
        }
      }

      while (Serial.available()) {
        Serial.read();
      }
    }
  }
}
