/*
  Example program of tcp acctions protocol.
  This program connects to a tcp server and listen for action mesages
  with this format: ~#ABC

   ~# = Header
   A = message type byte (1, action message, is the only type implemeted)
   B = action number byte (a value from 0 to 255 expresed in one byte)
   C = parameter byte (a value from 0 to 255 expresed in one byte)

  The TCPcomm class is designed for use a enc28j60 ethernet module with
  Nano expander v1 board. It needs UIPEthernet library.


*/

#include <Gallardcore_NanoExpander.h>

NANO_EXPANDER Brd;

void setup() {
  Serial.begin(9600);
  Brd.init();
}

void loop() {
  for (int i = 0; i < 6; i++) {
    Brd.setOut(i, 255);
    delay (1000);
  }
  for (int i = 0; i < 6; i++) {
    Brd.setOut(i, 0);
    delay (1000);
  }
}
