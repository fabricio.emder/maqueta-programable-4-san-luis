/*
  Example program of tcp acctions protocol.
  This program connects to a tcp server and listen for action mesages
  with this format: ~#ABC

   ~# = Header
   A = message type byte (1, action message, is the only type implemeted)
   B = action number byte (a value from 0 to 255 expresed in one byte)
   C = parameter byte (a value from 0 to 255 expresed in one byte)

  The TCPcomm class is designed for use a enc28j60 ethernet module with
  Nano expander v1 board. It needs UIPEthernet library.


*/

#include <Gallardcore_TCP_Protocol.h>
#include <Gallardcore_NanoExpander.h>

TCPcomm TCP;
NANO_EXPANDER Brd;

uint8_t mac[6] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05};
uint8_t actionState[5] = {0, 0, 0, 0, 0};
static uint8_t actionNumber[5] = {0, 1, 2, 3, 4};

void setup() {
#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
#endif

  /// Init tcp protocol.
  // mac, Arduino Ip, Server Ip
  TCP.init(mac, IPAddress(192, 168, 1, 65), IPAddress(192, 168, 1, 51), 9001);
  Brd.init();
}

void loop() {
  //Parse TCP messages and updates actionState with recived parameter corresponding
  //to the actionNumer on the array.
  TCP.parse(actionState, actionNumber);

  //Use the parameter recived for drive on outputs.
  for (int i = 0; i < 5; i++) {
    Brd.setOut(i, actionState[i]);
  }
}
