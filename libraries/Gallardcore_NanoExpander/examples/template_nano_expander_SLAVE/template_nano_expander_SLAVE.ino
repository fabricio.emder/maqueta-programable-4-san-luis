/*
  Example program of serial acctions protocol.
  This program parse serial action mesages with this format: ~#ABC

   ~# = Header
   A = message type byte (1, action message, is the only type implemeted)
   B = action number byte (a value from 0 to 255 expresed in one byte)
   C = parameter byte (a value from 0 to 255 expresed in one byte)
*/

#include <Gallardcore_NanoExpander.h>

NANO_EXPANDER Brd;

uint8_t actionState[6] = {0, 0, 0, 0, 0,0};
static uint8_t actionNumber[6] = {5, 6, 7, 8,9,10};

void setup() {
#if ACTLOGLEVEL>LOG_NONE
  LogObject.begin(9600);
  LogObject.print(F("LOG ON: "));
#endif

  Brd.init();
}

void loop() {
  //parse serial messages.
  Brd.parseSerial(actionState, actionNumber);

  //use the parameter recived for drive on outputs.
  for (int i = 0; i < 6; i++) {
    Brd.setOut(i, actionState[i]);
  }
}
