/*!
 NANO EXPANDER V 1.0.0
 libreria para placa arduino nano san luis.
*/

#include "Gallardcore_NanoExpander.h"
#include "Arduino.h"
#include "HardwareSerial.h"

uint8_t outPins[6] = {3,5,6,9,10,11};

NANO_EXPANDER::init(){
  for(uint8_t i = 0; i < 5;i++ ){
    pinMode(outPins[i],OUTPUT);
    digitalWrite(outPins[i],LOW);
  }

  #ifndef ETHERCON
  //D11 is used for ethernet SPI
  pinMode(outPins[5],OUTPUT);
  digitalWrite(outPins[5],LOW);
  #endif

  #ifndef IR_MOD
  pinMode(4,OUTPUT);
  digitalWrite(4,HIGH);
  #endif
}

NANO_EXPANDER::setOut(uint8_t out, uint8_t val){
  analogWrite(outPins[out-1],val);
}
NANO_EXPANDER::getIn(){
}

NANO_EXPANDER::getRawIn(){
}

NANO_EXPANDER::sensorsRead(){
}

NANO_EXPANDER::sendAction(uint8_t actNum, uint8_t parm){
  Serial.print("~#");
  Serial.write(1);
  Serial.write(actNum);
  Serial.write(parm);
}

NANO_EXPANDER::parseSerial(uint8_t *stateBuf,uint8_t *actionNumbers,uint8_t actionCount){
  if(Serial.available() > 2){
    while (Serial.available() > 4){
      //LogObject.print(Serial.available());
      if(Serial.read() == '~'){
        if(Serial.read() == '#'){
          byte msgType = Serial.read();

          if(msgType == 1){
            byte actionNumber = Serial.read();
            for(uint8_t i = 0; i < actionCount;i++){

              if(actionNumbers[i] == actionNumber){
                byte param = (uint8_t) Serial.read();
                stateBuf[i] = param;

                #if ACTLOGLEVEL>=LOG_INFO

                LogObject.print(F("ACTION_MSJ: "));
                LogObject.print(actionNumber);
                LogObject.print(F(" PARAM: "));
                LogObject.print(param);
                LogObject.println();

                #endif
              }
            }
          }
        }
      }
    }
  }
}

Desplazamiento::Desplazamiento(uint8_t homeSensorPin, uint8_t endSensorPin, uint8_t motorPinA, uint8_t motorPinB, uint8_t speed){
  _speed = speed;
  _motorPinA = motorPinA;
  _motorPinB = motorPinB;
  _homeSensorPin = homeSensorPin;
  _endSensorPin = endSensorPin;

  pinMode(_motorPinA,OUTPUT);
  digitalWrite(_motorPinA,LOW);
  pinMode(_motorPinB,OUTPUT);
  digitalWrite(_motorPinB,LOW);
}

Desplazamiento::go(){
  _state = 1;
}

Desplazamiento::state(){
  return _state;
}

Desplazamiento::tick(){
  switch (_state){
    case 0:
    //stop
    digitalWrite(_motorPinA,LOW);
    digitalWrite(_motorPinB,LOW);
    break;

    case 1:
    //ir
    digitalWrite(_motorPinA,_speed);
    digitalWrite(_motorPinB,LOW);
    //Serial.println(analogRead(_endSensorPin));

    if(analogRead(_endSensorPin) > _sensorTreshold) {
      _state = 2;
      _timer = millis();
    }
    break;

    case 2:
    //stop
    digitalWrite(_motorPinA,LOW);
    digitalWrite(_motorPinB,LOW);
    if(millis() - _timer > _timeout) _state = 3;
    break;

    case 3:
    //volver
    digitalWrite(_motorPinA,LOW);
    digitalWrite(_motorPinB,_speed);
    if(analogRead(_homeSensorPin) > _sensorTreshold) _state = 0;
    break;
  }
}
